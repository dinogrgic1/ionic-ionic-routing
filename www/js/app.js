// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.controller("tableController", function($scope, $rootScope, $ionicModal, $state, $http, $location) {
  var self = this;
  this.users = [];

  $http({
  method: 'GET'
  , url: '/users'
  }).then(function successCallback(response) {
    self.users = response["data"];
  }, function errorCallback(response) {
    
  });

  this.swipe = function(firstName, lastName) {
    var user = {firstName: firstName, lastName: lastName};
    $http({
      method: 'DELETE'
      , params: user
      , url: '/users'
    }).then(function successCallback(response) {
      self.users = response["data"];
    }, function errorCallback(response) {
      
    });
  }

  this.append = function(firstName, lastName) {
    var user = {firstName: firstName, lastName: lastName}
    createUser(user);
    $scope.closeModal();
  }

  this.goBack = function()
  {
    $rootScope.id = "";
    $rootScope.lastName = "";
    $rootScope.firstName = "";
    $location.path('/#/list');
    //$state.reload();
  }

  function createUser(user) {
    $http({
      method: 'POST'
      , data: user
      , url: '/users'
    }).then(function successCallback(response) {
      self.users = response["data"];
    }, function errorCallback(response) {
      
    });
  }

  $ionicModal.fromTemplateUrl('my-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.buttonAction = function() {
    console.log("buttonAction");
    $scope.modal.show();
  }

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  this.click = function(id, firstName, lastName)
  {
    $rootScope.id = id;
    $rootScope.firstName = firstName;
    $rootScope.lastName = lastName;
  }

  this.update = function(firstName, lastName)
  {
    var user = {id:$rootScope.id, firstName:firstName, lastName:lastName};
    updateUser(user);

  }

  function updateUser(user)
  {
    var user = user;
    $http({
      method: 'PUT',
      data: user,
      url : '/update'
    }).then(function successCallback(response){
      console.log('success');
      self.users = response.data;
      console.log(self.users);
    }, function errorCallback(response) {
      
    });
  }
})

.config(function($stateProvider, $urlRouterProvider) {  
  $stateProvider
  .state('list', {
    cache : false,
    url: '/list',
    templateUrl: 'views/list.html',
    controller: 'tableController as tableCtrl'
  })
  .state('update', {
    cache : false,
    url: '/list/update',
    templateUrl: 'views/listUpdate.html',
    controller: 'tableController as tableCtrl'
  });
  $urlRouterProvider.otherwise('/list');
});