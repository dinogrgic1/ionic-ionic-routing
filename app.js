//app.js i www se namaze u istom folderu (root)
//npm install express body-parser --save
//npm install nodemon -g
//nodemon app.js
//firefox -> localhost:3000

var express = module.require('express');
var bodyParser = module.require('body-parser')
var app = express();

var users = [
	{id: 1, firstName: "f1", lastName: "l1"}
	, {id: 2, firstName: "f2", lastName: "l2"}
]

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));

app.use(express.static(__dirname + '/www'));

app.get("/users", function(req, res) {
	res.json(users);
});

app.post("/users", function(req, res) {
	var firstName = req.body.firstName;
	var lastName = req.body.lastName;
	var user = {firstName: firstName, lastName: lastName};
	user.id = users.length + 1;
	users.push(user);
	res.json(users);
});

app.delete("/users", function(req, res) {
	var firstName = req.query.firstName;
	var lastName = req.query.lastName;

	var userIndex = -1;
	for (index in users) {
		var user = users[index];
		if (user.firstName == firstName && user.lastName == lastName) {
			userIndex = index;
			break;
		}
	}

	if (userIndex != -1) {
		users.splice(userIndex, 1);
	}

	res.json(users);

});

app.put('/update', function(req,res){
	var id = req.body.id;
	var firstName = req.body.firstName;
	var lastName = req.body.lastName;
	
	var user = {id:id, firstName:firstName, lastName:lastName};
	console.log(user);
	for(var i = 0; i < users.length; i++)
	{
		if(users[i].id == id)
		{
			users[i] = user;
			console.log(users);
		}
	}
	res.json(users);
});

app.listen(3000, function(){
	console.log("listening on 3000");
});